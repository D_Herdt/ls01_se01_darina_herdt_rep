import java.util.Scanner;

public class PCHaendler {
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String ausgabe = myScanner.next();
	
		return ausgabe;

	}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in) ;
		System.out.println(text);
		int ausgabe = myScanner.nextInt();
		
		return ausgabe;
}
	public static double liesdouble(String text) {
		Scanner myScanner = new Scanner(System.in) ;
		System.out.println(text);
		double ausgabe = myScanner.nextDouble();
		
		return ausgabe;
}

	public static void main(String[] args) {
		//Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		double preis = liesdouble("Geben Sie den Nettopreis ein:");
		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		double mwst = liesdouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		


		// Ausgeben

		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			 double erg = anzahl * nettopreis;
			 return erg;
			 
		}
		public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
			double erg = nettogesamtpreis * (1 + mwst / 100);
			return erg; 
		}
		
		public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		}
}
